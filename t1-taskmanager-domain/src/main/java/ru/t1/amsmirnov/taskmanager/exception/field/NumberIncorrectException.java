package ru.t1.amsmirnov.taskmanager.exception.field;

public final class NumberIncorrectException extends AbstractFieldException {

    public NumberIncorrectException() {
        super("Error! Number is incorrect...");
    }

    public NumberIncorrectException(final String value, final Throwable cause) {
        super("Error! This " + value + " is not number...", cause);
    }

}
