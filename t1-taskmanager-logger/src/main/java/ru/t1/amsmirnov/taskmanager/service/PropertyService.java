package ru.t1.amsmirnov.taskmanager.service;

import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.amsmirnov.taskmanager.api.IPropertyService;

import java.io.IOException;
import java.util.Properties;

@Service
public final class PropertyService implements IPropertyService {

    @NotNull
    private static final Logger logger = Logger.getLogger(PropertyService.class);

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String LOGGER_MONGODB_HOST = "mongodb.host";

    @NotNull
    private static final String LOGGER_MONGODB_HOST_DEFAULT = "0.0.0.0";

    @NotNull
    private static final String LOGGER_MONGODB_PORT = "mongodb.port";

    @NotNull
    private static final String LOGGER_MONGODB_PORT_DEFAULT = "27017";

    @NotNull
    private static final String LOGGER_MONGODB_NAME = "mongodb.name";

    @NotNull
    private static final String LOGGER_MONGODB_NAME_DEFAULT = "tm_logger";

    @NotNull
    private static final String LOGGER_MONGODB_COLLECTION_NAME = "mongodb.default_collection_name";

    @NotNull
    private static final String LOGGER_MONGODB_COLLECTION_NAME_DEFAULT = "tm_default";

    @NotNull
    private static final String MQ_BROKER_HOST = "broker.host";

    @NotNull
    private static final String MQ_BROKER_HOST_DEFAULT = "0.0.0.0";

    @NotNull
    private static final String MQ_BROKER_PORT = "broker.port";

    @NotNull
    private static final String MQ_BROKER_PORT_DEFAULT = "61616";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private final Properties properties = new Properties();

    @NotNull
    private Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().contains(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }


    public PropertyService() {
        try {
            properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
        } catch (final IOException e) {
            logger.warn("Error while reading application.properties file!");
            logger.warn(e);
        }
    }

    @NotNull
    @Override
    public String getMongoDBHost() {
        return getStringValue(LOGGER_MONGODB_HOST, LOGGER_MONGODB_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getMongoDBPort() {
        return getIntegerValue(LOGGER_MONGODB_PORT, LOGGER_MONGODB_PORT_DEFAULT);
    }

    @NotNull
    @Override
    public String getMongoDBName() {
        return getStringValue(LOGGER_MONGODB_NAME, LOGGER_MONGODB_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getMongoDBDefCollectionName() {
        return getStringValue(LOGGER_MONGODB_COLLECTION_NAME, LOGGER_MONGODB_COLLECTION_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getBrokerPort() {
        return getIntegerValue(MQ_BROKER_PORT, MQ_BROKER_PORT_DEFAULT);
    }

    @NotNull
    @Override
    public  String getBrokerHost() {
        return getStringValue(MQ_BROKER_HOST, MQ_BROKER_HOST_DEFAULT);
    }

}
