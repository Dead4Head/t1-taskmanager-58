package ru.t1.amsmirnov.taskmanager.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.amsmirnov.taskmanager.api.ILoggerService;
import ru.t1.amsmirnov.taskmanager.api.IPropertyService;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

@Service
public final class LoggerService implements ILoggerService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    private final MongoClient mongoClient;

    @NotNull
    private final MongoDatabase mongoDatabase;

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    public LoggerService() {
        mongoClient = new MongoClient(propertyService.getMongoDBHost(), propertyService.getMongoDBPort());
        mongoDatabase = mongoClient.getDatabase(propertyService.getMongoDBName());
    }

    @Override
    public void log(@NotNull final String message) throws IOException {
        @NotNull final Map<String, Object> event = objectMapper.readValue(message, LinkedHashMap.class);
        @Nullable String collectionName = event.get("tableName").toString();
        if (collectionName == null || collectionName.isEmpty())
            collectionName = propertyService.getMongoDBDefCollectionName();
        if (mongoDatabase.getCollection(collectionName) == null)
            mongoDatabase.createCollection(collectionName);
        @NotNull final MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
        collection.insertOne(new Document(event));
    }

}
