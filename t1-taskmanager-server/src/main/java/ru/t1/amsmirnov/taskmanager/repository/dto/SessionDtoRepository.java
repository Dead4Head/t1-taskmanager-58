package ru.t1.amsmirnov.taskmanager.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.amsmirnov.taskmanager.api.repository.dto.ISessionDtoRepository;
import ru.t1.amsmirnov.taskmanager.dto.model.SessionDTO;

import java.util.List;

@Repository
@Scope("prototype")
public final class SessionDtoRepository extends AbstractUserOwnedDtoRepository<SessionDTO> implements ISessionDtoRepository {

    public SessionDtoRepository() {
        super();
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll() {
        final String jql = "SELECT s FROM SessionDTO s ORDER BY s.created";
        return entityManager.createQuery(jql, SessionDTO.class).getResultList();
    }

    @NotNull
    @Override
    public List<SessionDTO> findAllSorted(@Nullable final String sort) {
        if (sort == null || sort.isEmpty()) return findAll();
        final String jql = "SELECT s FROM SessionDTO s ORDER BY s." + sort;
        return entityManager.createQuery(jql, SessionDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll(@NotNull final String userId) {
        final String jql = "SELECT s FROM SessionDTO s WHERE s.userId = :userId ORDER BY s.created";
        return entityManager.createQuery(jql, SessionDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<SessionDTO> findAllSorted(
            @NotNull final String userId,
            @Nullable final String sort
    ) {
        if (sort == null || sort.isEmpty()) return findAll(userId);
        final String jql = "SELECT s FROM SessionDTO s WHERE s.userId = :userId ORDER BY s." + sort;
        return entityManager.createQuery(jql, SessionDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@NotNull final String id) {
        final String jql = "SELECT s FROM SessionDTO s WHERE s.id = :id";
        return entityManager.createQuery(jql, SessionDTO.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public SessionDTO findOneById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        final String jql = "SELECT s FROM SessionDTO s WHERE s.userId = :userId AND s.id = :id";
        return entityManager.createQuery(jql, SessionDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);

    }

    @Override
    public void removeAll() {
        @NotNull final String jql = "DELETE FROM SessionDTO";
        entityManager.createQuery(jql).executeUpdate();
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @NotNull final String jql = "DELETE FROM SessionDTO s WHERE s.userId = :userId";
        entityManager.createQuery(jql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
