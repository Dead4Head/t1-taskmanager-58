package ru.t1.amsmirnov.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.amsmirnov.taskmanager.api.endpoint.IAuthEndpoint;
import ru.t1.amsmirnov.taskmanager.api.endpoint.IProjectEndpoint;
import ru.t1.amsmirnov.taskmanager.dto.model.ProjectDTO;
import ru.t1.amsmirnov.taskmanager.dto.request.project.*;
import ru.t1.amsmirnov.taskmanager.dto.request.user.UserLoginRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.project.*;
import ru.t1.amsmirnov.taskmanager.dto.response.user.UserLoginResponse;
import ru.t1.amsmirnov.taskmanager.enumerated.ProjectSort;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.marker.SoapCategory;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Category(value = SoapCategory.class)
public class ProjectEndpointTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String NONE_STR = "---NONE---";

    @NotNull
    private final static IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @NotNull
    private final static IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @NotNull
    private final static String login = "test";

    @NotNull
    private final static String password = "test";

    @NotNull
    private List<ProjectDTO> projects = new ArrayList<>();

    @Nullable
    private static String token;

    @BeforeClass
    public static void login() {
        @NotNull final UserLoginRequest request = new UserLoginRequest(login, password);
        @NotNull final UserLoginResponse response = authEndpoint.login(request);
        token = response.getToken();
    }

    @Before
    public void initData() {
        projects = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(new ProjectCreateRequest(token, "TEST ProjectDTO " + i, NONE_STR));
            projects.add(response.getProject());
        }
    }

    @After
    public void clearData() {
        projectEndpoint.removeAllProjects(new ProjectClearRequest(token));
    }

    @Test
    public void testProjectChangeStatusById() {
        @NotNull ProjectChangeStatusByIdRequest request;
        @NotNull ProjectChangeStatusByIdResponse response;
        for (@NotNull final ProjectDTO project : projects) {
            request = new ProjectChangeStatusByIdRequest(token, project.getId(), Status.IN_PROGRESS);
            response = projectEndpoint.changeProjectStatusById(request);
            assertTrue(response.isSuccess());
            assertNotEquals(project.getStatus(), response.getProject().getStatus());
        }

        request = new ProjectChangeStatusByIdRequest(NONE_STR, NONE_STR, Status.IN_PROGRESS);
        response = projectEndpoint.changeProjectStatusById(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());

        request = new ProjectChangeStatusByIdRequest(token, NONE_STR, Status.IN_PROGRESS);
        response = projectEndpoint.changeProjectStatusById(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
    }


    @Test
    public void testRemoveAll() {
        @NotNull ProjectShowByIdRequest projectRequest = new ProjectShowByIdRequest(token, projects.get(0).getId());
        @NotNull ProjectShowByIdResponse projectResponse = projectEndpoint.findOneProjectById(projectRequest);
        assertNotNull(projectResponse.getProject());

        @NotNull ProjectClearRequest request = new ProjectClearRequest(token);
        @NotNull ProjectClearResponse response = projectEndpoint.removeAllProjects(request);
        assertTrue(response.isSuccess());

        projectRequest = new ProjectShowByIdRequest(token, projects.get(0).getId());
        projectResponse = projectEndpoint.findOneProjectById(projectRequest);
        assertNull(projectResponse.getProject());

        request = new ProjectClearRequest(NONE_STR);
        response = projectEndpoint.removeAllProjects(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testProjectCompleteById() {
        @NotNull ProjectCompleteByIdRequest request;
        @NotNull ProjectCompleteByIdResponse response;
        for (@NotNull final ProjectDTO project : projects) {
            request = new ProjectCompleteByIdRequest(token, project.getId());
            response = projectEndpoint.completeProjectById(request);
            assertEquals(Status.COMPLETED, response.getProject().getStatus());
            assertTrue(response.isSuccess());
        }

        request = new ProjectCompleteByIdRequest(NONE_STR, projects.get(0).getId());
        response = projectEndpoint.completeProjectById(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());

        new ProjectCompleteByIdRequest(token, NONE_STR);
        response = projectEndpoint.completeProjectById(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testProjectCreate() {
        @NotNull final String name = "Test project";
        @NotNull final String description = "description";
        @NotNull ProjectCreateRequest request = new ProjectCreateRequest(token, name, description);
        @NotNull ProjectCreateResponse response = projectEndpoint.createProject(request);
        assertEquals(name, response.getProject().getName());
        assertEquals(description, response.getProject().getDescription());
        assertEquals(Status.NOT_STARTED, response.getProject().getStatus());
        assertTrue(response.isSuccess());

        response = projectEndpoint.createProject(new ProjectCreateRequest(NONE_STR, name, description));
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());

        response = projectEndpoint.createProject(new ProjectCreateRequest(token, "", description));
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
    }


    @Test
    public void testFindAllProjects() {
        @NotNull ProjectListRequest request = new ProjectListRequest(token, ProjectSort.BY_DEFAULT);
        @NotNull ProjectListResponse response = projectEndpoint.findAllProjects(request);
        assertEquals(projects, response.getProjects());

        request = new ProjectListRequest(NONE_STR, ProjectSort.BY_DEFAULT);
        response = projectEndpoint.findAllProjects(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testProjectRemoveById() {
        @NotNull ProjectRemoveByIdRequest removeRequest;
        @NotNull ProjectRemoveByIdResponse removeResponse;
        @NotNull ProjectShowByIdRequest findRequest;
        @NotNull ProjectShowByIdResponse findResponse;
        for (@NotNull final ProjectDTO project : projects) {
            removeRequest = new ProjectRemoveByIdRequest(token, project.getId());
            removeResponse = projectEndpoint.removeOneProjectById(removeRequest);
            assertTrue(removeResponse.isSuccess());
            findRequest = new ProjectShowByIdRequest(token, project.getId());
            findResponse = projectEndpoint.findOneProjectById(findRequest);
            assertNull(findResponse.getProject());
        }

        removeRequest = new ProjectRemoveByIdRequest(NONE_STR, NONE_STR);
        removeResponse = projectEndpoint.removeOneProjectById(removeRequest);
        assertFalse(removeResponse.isSuccess());
        assertNotNull(removeResponse.getMessage());
    }

    @Test
    public void testProjectFindById() {
        @NotNull ProjectShowByIdRequest request;
        @NotNull ProjectShowByIdResponse response;
        for (@NotNull final ProjectDTO project : projects) {
            request = new ProjectShowByIdRequest(token, project.getId());
            response = projectEndpoint.findOneProjectById(request);
            assertEquals(project, response.getProject());
        }

        request = new ProjectShowByIdRequest(NONE_STR, NONE_STR);
        response = projectEndpoint.findOneProjectById(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testProjectStartById() {
        @NotNull ProjectStartByIdRequest request;
        @NotNull ProjectStartByIdResponse response;
        for (@NotNull final ProjectDTO project : projects) {
            request = new ProjectStartByIdRequest(token, project.getId());
            response = projectEndpoint.startProjectById(request);
            assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());
        }
        request = new ProjectStartByIdRequest(NONE_STR, NONE_STR);
        response = projectEndpoint.startProjectById(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());

        request = new ProjectStartByIdRequest(token, NONE_STR);
        response = projectEndpoint.startProjectById(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testProjectUpdateById() {
        @NotNull ProjectUpdateByIdRequest request;
        @NotNull ProjectUpdateByIdResponse response;
        for (final ProjectDTO project : projects) {
            @NotNull final String name = project.getName() + " TEST";
            @NotNull final String description = project.getDescription() + " TEST";
            request = new ProjectUpdateByIdRequest(token, project.getId(), name, description);
            response = projectEndpoint.updateProjectById(request);
            assertEquals(name, response.getProject().getName());
            assertEquals(description, response.getProject().getDescription());
        }
        request = new ProjectUpdateByIdRequest(token, NONE_STR, NONE_STR, NONE_STR);
        response = projectEndpoint.updateProjectById(request);
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());
    }

}
