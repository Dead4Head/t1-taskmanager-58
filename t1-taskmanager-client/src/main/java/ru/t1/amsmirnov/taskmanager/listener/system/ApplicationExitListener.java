package ru.t1.amsmirnov.taskmanager.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.event.ConsoleEvent;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

@Component
public final class ApplicationExitListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "exit";

    @NotNull
    public static final String DESCRIPTION = "Close TaskDTO Manager.";

    @Nullable
    public static final String ARGUMENT = null;

    @Nullable
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@applicationExitListener .isSystemCommand(#consoleEvent.name)")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.exit(0);
    }

}
